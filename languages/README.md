# Simple Settings

This folder contains translation files for Simple Settings.

Do not store custom translations in this folder, they will be deleted on updates.
Store custom translations in `wp-content/languages/simple-settings`.
