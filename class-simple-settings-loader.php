<?php
/**
 * Plugin Name:     Simple Settings
 * Plugin URI:      https://widgit.io
 * Description:     A library for implementing a simple, transparent settings panel in WordPress.
 * Author:          Widgit Team
 * Author URI:      https://widgit.io
 * Version:         2.0.0
 * Text Domain:     simple-settings-demo
 * Domain Path:     languages
 *
 * @package         Widgit\SimpleSettings\Bootstrap
 * @author          Daniel J Griffiths <dgriffiths@evertiro.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Conditionally load the library.
if ( ! class_exists( 'Simple_Settings' ) ) {
	$base_dir = trailingslashit( dirname( __FILE__ ) );

	if ( file_exists( $base_dir . 'class-simple-settings.php' ) ) {
		require_once $base_dir . 'class-simple-settings.php';
	} else {
		$files = glob( $base_dir . '**/class-simple-settings.php' );

		if ( 1 === count( $files ) ) {
			require_once $files[0];
		}
	}
}


// Conditionally load the demo.
if ( ! class_exists( 'Simple_Settings_Demo' ) && simple_settings_get_demo_status() ) {
	$base_dir = trailingslashit( dirname( __FILE__ ) );
	$files    = glob( $base_dir . '**/class-simple-settings-demo.php' );

	if ( 1 === count( $files ) ) {
		require_once $files[0];
	}
}


/**
 * Add plugin row meta.
 *
 * @since       2.0.0
 * @param       array  $row_meta The current plugin meta row.
 * @param       string $plugin_file The plugin file.
 * @return      array  $row_meta The updated plugin meta row
 */
function simple_settings_plugin_row_meta( $row_meta, $plugin_file ) {
	if ( basename( __FILE__ ) === basename( $plugin_file ) && current_user_can( 'update_plugins' ) ) {

		$link_url   = wp_nonce_url( add_query_arg( array( 'simple-settings-action' => 'toggle_demo' ) ) );
		$link_label = ( simple_settings_get_demo_status() ) ? __( 'Disable Demo Mode', 'simple-settings' ) : __( 'Enable Demo Mode', 'simple-settings' );


		$row_meta[] = sprintf( '<a href="%s">%s</a>', $link_url, $link_label );
	}

	return $row_meta;
}
add_filter( 'plugin_row_meta', 'simple_settings_plugin_row_meta', 10, 2 );


/**
 * Get the current status of demo mode.
 *
 * @since       2.0.0
 * @return      bool $enabled True if enabled, false otherwise
 */
function simple_settings_get_demo_status() {
	if ( is_network_admin() ) {
		return (bool) get_network_option( null, 'simple_settings_demo' );
	}

	return (bool) get_option( 'simple_settings_demo' );
}


/**
 * Toggle the status of demo mode.
 *
 * @since       2.0.0
 * @return      bool $enabled True if enabled, false otherwise
 */
function simple_settings_toggle_demo_status() {
	$enabled = simple_settings_get_demo_status();

	if ( is_network_admin() ) {
		return update_network_option( null, 'simple_settings_demo', ! $enabled );
	}

	return update_option( 'simple_settings_demo', ! $enabled );
}


/**
 * Display notice on demo toggle
 *
 * @since       2.0.0
 * @return      void
 */
function simple_settings_demo_mode_notice() {
	if ( isset( $_REQUEST['simple_settings_nonce'] ) ) {
		check_admin_referer( 'simple_settings_nonce', 'simple_settings_nonce' );
	}

	$get = wp_unslash( $_GET );

	if ( isset( $get['updated'] ) && 'simple-settings-demo' === $get['updated'] ) {
		$status = ( simple_settings_get_demo_status() ) ? __( 'Simple Settings demo mode enabled.', 'simple-settings' ) : __( 'Simple Settings demo mode disabled.', 'simple-settings' );

		echo '<div id="message" class="updated notice is-dismissible"><p>' . esc_html( $status ) . '</p></div>';
	}
}
add_action( 'admin_head', 'simple_settings_demo_mode_notice' );


/**
 * Toggle demo mode
 *
 * @since       2.0.0
 * @return      void
 */
function simple_settings_toggle_demo() {
	simple_settings_toggle_demo_status();
	wp_safe_redirect(
		add_query_arg(
			array(
				'updated'                => 'simple-settings-demo',
				'simple-settings-action' => false,
				'_wpnonce'               => false,
			)
		)
	);
	exit;
}
add_action( 'simple_settings_toggle_demo', 'simple_settings_toggle_demo' );


/**
 * Processes all actions sent via POST and GET by looking for the 'simple-settings-action'
 * request and running do_action() to call the function
 *
 * @since       2.0.0
 * @return      void
 */
function simple_settings_process_actions() {
	if ( isset( $_REQUEST['simple_settings_nonce'] ) ) {
		check_admin_referer( 'simple_settings_nonce', 'simple_settings_nonce' );
	}

	$post = wp_unslash( $_POST );

	if ( isset( $post['simple-settings-action'] ) ) {
		do_action( 'simple_settings_' . $post['simple-settings-action'], $post );
	}

	$get = wp_unslash( $_GET );

	if ( isset( $get['simple-settings-action'] ) ) {
		do_action( 'simple_settings_' . $get['simple-settings-action'], $get );
	}
}
add_action( 'admin_init', 'simple_settings_process_actions' );
