# Simple Settings

[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)](https://gitlab.com/widgitlabs/wordpress/simple-settings/blob/master/license.txt)
[![Pipelines](https://gitlab.com/widgitlabs/wordpress/simple-settings/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/wordpress/simple-settings/pipelines)
[![Packagist](https://img.shields.io/packagist/v/widgitlabs/simple-settings.svg)](https://packagist.org/packages/widgitlabs/simple-settings)
[![Packagist](https://img.shields.io/packagist/dt/widgitlabs/simple-settings.svg)](https://packagist.org/packages/widgitlabs/simple-settings)
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)](https://discord.gg/jrydFBP)

> Simple Settings has been superceded by [Origami](https://gitlab.com/widgitlabs/wordpress/origami).

## What Is This

Depending on the size of a project, you may be able to get away with adding
settings to an existing WordPress page (or the customizer). On the other end
of the spectrum, you may need (or want) to implement a full-scale control panel
like Redux. However, what if your project is somewhere in the middle?
Alternatively, what if you need a control panel but don't want the bulk that
goes along with most? This need is something that I've struggled with for some
time. Simple Settings is a simple, standards-compliant library for creating
theme and plugin settings panels with a difference. Built on the core of the
excellent system used by Easy Digital Downloads, Simple Settings strives to be
simple, extensible, and maintain a native look and feel.

_**Note:** The license field is horribly outdated and should not be used in
production without extensive testing until I have access to SL again and can
properly update it._

More information can be found on the [wiki](https://gitlab.com/widgitlabs/wordpress/simple-settings/wikis/home).

## Installation

### Composer

Simple Settings can be included in your project using [Composer](https://getcomposer.org/)
by adding it to your `composer.json`:

```json
{
    "require-dev": {
        "widgitlabs/simple-settings": "*"
    }
}
```

### Standalone

Simple Settings can be included manually by downloading the latest release from
the [Releases page](https://gitlab.com/widgitlabs/wordpress/simple-settings/-/releases)
and extracting it in your project directory.

## Demo Mode

Simple Settings installations only include the actual settings library, but for
testing and development use we also provide a full WordPress plugin including a
"Demo Mode" which can be activated through the WordPress Plugins page. This
option is currently _only_ available through GitLab.

### Prebuilt Package

For every release we provide a prebuilt version of the WordPress plugin, which
can be downloaded through the [GitLab Package Registry](https://gitlab.com/widgitlabs/wordpress/simple-settings/-/packages).

### Git

```sh
$ git clone https://gitlab.com/widgitlabs/wordpress/simple-settings.git

# or

$ git submodule add https://gitlab.com/widgitlabs/wordpress/simple-settings.git
```

## Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/simple-settings/issues)!

## Contributions

Anyone is welcome to contribute to the library. Please read the
[guidelines for contributing](https://github.com/widgitlabs/wordpress/simple-settings/blob/master/CONTRIBUTING.md)
to this repository.

There are various ways you can contribute:

1. Raise an [Issue](https://gitlab.com/widgitlabs/wordpress/simple-settings/issues)
   on GitLab
2. Send us a Pull Request with your bug fixes and/or new features
3. Provide feedback and suggestions on [enhancements](https://gitlab.com/widgitlabs/wordpress/simple-settings/issues?label_name[]=Enhancement)
4. Help [translate](https://poeditor.com/join/project?hash=klXrYxkszL)
   Simple Settings
