<?php
/**
 * Loader unit tests
 *
 * @package     SimpleSettings\Tests\Loader
 * @since       2.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Loader unit tests
 *
 * @since       2.0.0
 *
 * @uses ::simple_settings_demo
 */
class Tests_Simple_Settings_Loader extends WP_UnitTestCase {
	// This is not a core file and we can't control non-WordPress code.
	// phpcs:disable WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       2.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = simple_settings_demo();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::tearDown();
	}


	/**
	 * Test simple_settings_get_demo_status
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 * @covers      ::simple_settings_get_demo_status
	 * @uses        Simple_Settings_Demo::instance
	 * @uses        ::simple_settings_demo
	 */
	public function test_simple_settings_get_demo_status() {
		// TODO: Make sure ALL tests support network.
		$status = (bool) get_option( 'simple_settings_demo' );
		if ( is_network_admin() ) {
			$status = (bool) get_network_option( null, 'simple_settings_demo' );
		}

		$this->assertSame( $status, simple_settings_get_demo_status() );
	}


	/**
	 * Test simple_settings_toggle_demo_status
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 * @covers      ::simple_settings_toggle_demo_status
	 * @uses        Simple_Settings_Demo::instance
	 * @uses        ::simple_settings_demo
	 * @uses        ::simple_settings_get_demo_status
	 */
	public function test_simple_settings_toggle_demo_status() {
		$this->assertSame( true, (bool) simple_settings_get_demo_status() );
		simple_settings_toggle_demo_status();
		$this->assertSame( false, (bool) simple_settings_get_demo_status() );
		simple_settings_toggle_demo_status();
	}


	/**
	 * Test simple_settings_demo
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 * @covers      ::simple_settings_demo
	 * @uses        Simple_Settings::get_version
	 * @uses        Simple_Settings_Demo::instance
	 */
	public function test_simple_settings_demo() {
		$settings_object = simple_settings_demo();

		$this->assertEquals( '2.0.0', $settings_object->settings->get_version() );
	}
}
