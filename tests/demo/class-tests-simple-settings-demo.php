<?php
/**
 * Core unit tests
 *
 * @package     SimpleSettings\Tests\Demo
 * @since       2.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       2.0.0
 */
class Tests_Simple_Settings_Demo extends Tests_Simple_Settings_Loader {
	// This is not a core file and we can't control non-WordPress code.
	// phpcs:disable WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 */
	public function setUp() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::setUp();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::tearDown();
	}


	/**
	 * Test instance
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 * @covers      Simple_Settings_Demo::instance
	 * @covers      Simple_Settings_Demo::setup_constants
	 * @uses        ::simple_settings_demo
	 */
	public function test_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'Simple_Settings_Demo' );
		$this->assertIsCallable( 'simple_settings_demo' );

		$plugin_url = trailingslashit( str_replace( 'tests/demo/', '', plugin_dir_url( __FILE__ ) ) );
		$this->assertSame( SIMPLE_SETTINGS_URL, $plugin_url );

		$plugin_path  = str_replace( 'tests/demo/', '', plugin_dir_path( __FILE__ ) );
		$plugin_path  = trailingslashit( substr( $plugin_path, 0, -1 ) );
		$compare_path = trailingslashit( substr( SIMPLE_SETTINGS_DIR, 0, -1 ) );
		$this->assertSame( $plugin_path, $compare_path );

		$this->assertFileExists( $plugin_path . 'CHANGELOG.md' );
		$this->assertFileExists( $plugin_path . 'class-simple-settings-loader.php' );
		$this->assertFileExists( $plugin_path . 'class-simple-settings.php' );
		$this->assertFileExists( $plugin_path . 'CONTRIBUTING.md' );
		$this->assertFileExists( $plugin_path . 'license.txt' );
		$this->assertFileExists( $plugin_path . 'README.md' );
		$this->assertFileExists( $plugin_path . 'assets/css/admin.css' );
		$this->assertFileExists( $plugin_path . 'assets/css/admin.min.css' );
		$this->assertFileExists( $plugin_path . 'assets/css/jquery-ui-classic.min.css' );
		$this->assertFileExists( $plugin_path . 'assets/css/jquery-ui-fresh.min.css' );
		$this->assertFileExists( $plugin_path . 'assets/css/select2.css' );
		$this->assertFileExists( $plugin_path . 'assets/css/select2.min.css' );
		$this->assertFileExists( $plugin_path . 'assets/js/admin.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/admin.min.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2.full.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2.full.min.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2.min.js' );
		$this->assertFileExists( $plugin_path . 'demo/class-simple-settings-demo.php' );
		$this->assertFileExists( $plugin_path . 'demo/register-settings.php' );
		$this->assertFileExists( $plugin_path . 'demo/assets/css/font.css' );
		$this->assertFileExists( $plugin_path . 'demo/assets/css/font.min.css' );
		$this->assertFileExists( $plugin_path . 'demo/assets/font/simple-settings.eot' );
		$this->assertFileExists( $plugin_path . 'demo/assets/font/simple-settings.svg' );
		$this->assertFileExists( $plugin_path . 'demo/assets/font/simple-settings.ttf' );
		$this->assertFileExists( $plugin_path . 'demo/assets/font/simple-settings.woff' );
		$this->assertFileExists( $plugin_path . 'demo/assets/font/simple-settings.woff2' );
		$this->assertFileExists( $plugin_path . 'languages/README.md' );
		$this->assertFileExists( $plugin_path . 'languages/simple-settings-en_GB.mo' );
		$this->assertFileExists( $plugin_path . 'languages/simple-settings.mo' );
		$this->assertFileExists( $plugin_path . 'languages/simple-settings.pot' );
		$this->assertFileExists( $plugin_path . 'modules/licensing/class-simple-settings-licensing.php' );
		$this->assertFileExists( $plugin_path . 'modules/licensing/class-simple-settings-plugin-updater.php' );
		$this->assertFileExists( $plugin_path . 'modules/sysinfo/class-browser.php' );
		$this->assertFileExists( $plugin_path . 'modules/sysinfo/class-simple-settings-sysinfo.php' );
	}
}
