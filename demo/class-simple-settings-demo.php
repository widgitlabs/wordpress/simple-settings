<?php
/**
 * Simple Settings Demo
 *
 * @package         Widgit\SimpleSettings\Demo
 * @since           2.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Simple_Settings_Demo' ) ) {


	/**
	 * Main Simple_Settings_Demo class
	 *
	 * @access      public
	 * @since       2.0.0
	 */
	final class Simple_Settings_Demo {


		/**
		 * The one true Simple_Settings_Demo
		 *
		 * @access      private
		 * @since       2.0.0
		 * @var         Simple_Settings_Demo $instance The one true Simple_Settings_Demo
		 */
		private static $instance;


		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       2.0.0
		 * @var         object $settings The settings object
		 */
		public $settings;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       2.0.0
		 * @static
		 * @return      object self::$instance The one true Simple_Settings_Demo
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Simple_Settings_Demo ) ) {
				self::$instance = new Simple_Settings_Demo();
				self::$instance->setup_constants();
				self::$instance->includes();
				self::$instance->hooks();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       2.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'simple-settings' ), '2.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       2.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'simple-settings' ), '2.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       2.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin path.
			if ( ! defined( 'SIMPLE_SETTINGS_DIR' ) ) {
				$path = str_replace( 'demo/', '', plugin_dir_path( __FILE__ ) );
				$path = trailingslashit( substr( $path, 0, -1 ) );

				define( 'SIMPLE_SETTINGS_DIR', $path );
			}

			// Plugin URL.
			if ( ! defined( 'SIMPLE_SETTINGS_URL' ) ) {
				$path = trailingslashit( str_replace( 'demo/', '', plugin_dir_url( __FILE__ ) ) );

				define( 'SIMPLE_SETTINGS_URL', $path );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       2.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
			add_action( 'admin_enqueue_scripts', array( self::$instance, 'enqueue_scripts' ), 100 );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       2.0.0
		 * @return      void
		 */
		private function includes() {
			global $simple_settings_demo_options;

			require_once SIMPLE_SETTINGS_DIR . 'demo/register-settings.php';

			$location = is_network_admin() ? 'network' : '';

			self::$instance->settings     = new Simple_Settings( 'simple-settings-demo', 'core', array( 'sysinfo' ), $location );
			$simple_settings_demo_options = self::$instance->settings->get_settings();
		}


		/**
		 * Enqueue scripts
		 *
		 * @access      public
		 * @since       2.0.0
		 * @return      void
		 */
		public function enqueue_scripts() {
			// Use minified libraries if SCRIPT_DEBUG is turned off.
			$suffix   = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
			$url_path = str_replace( WP_CONTENT_DIR, WP_CONTENT_URL, dirname( __FILE__ ) );

			// If working on Windows, we have to be hacky.
			if ( strtoupper( substr( PHP_OS, 0, 3 ) ) === 'WIN' ) {
				$url_path = str_replace( '\\', '/', str_replace( str_replace( '/', '\\', WP_CONTENT_DIR ), WP_CONTENT_URL, $url_path ) );
				$url_data = wp_parse_url( $url_path );
				$url_path = $url_data['path'];
			}

			// Setup versioning for internal assets.
			$css_ver = gmdate( 'ymd-Gis', filemtime( trailingslashit( dirname( __FILE__ ) ) . 'assets/css/font' . $suffix . '.css' ) );

			wp_enqueue_style( 'simple-settings-font', $url_path . '/assets/css/font' . $suffix . '.css', array(), self::$instance->settings->get_version() . '-' . $css_ver );
		}


		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       2.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'simple_settings_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'simple_settings' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'simple_settings', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/simple-settings/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/simple-settings/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/simple-settings folder.
				load_textdomain( 'simple_settings', $mofile_global );
				return;
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/simple-settings/languages/ folder.
				load_textdomain( 'simple_settings', $mofile_local );
				return;
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/simple-settings/ folder.
				load_textdomain( 'simple_settings', $mofile_core );
				return;
			}

			// Load the default language files.
			load_plugin_textdomain( 'simple_settings', false, $lang_dir );
		}
	}
}


/**
 * The main function responsible for returning the one true Simple_Settings_Demo
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $Simple_Settings_Demo = simple_settings_demo(); ?>
 *
 * @since       2.0.0
 * @return      Simple_Settings_Demo The one true Simple_Settings_Demo
 */
function simple_settings_demo() {
	return Simple_Settings_Demo::instance();
}

// Get things started.
simple_settings_demo();
