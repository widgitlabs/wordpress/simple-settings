<?php
/**
 * Register settings
 *
 * @package     SimpleSettings\Demo\Admin\Settings\Register
 * @since       2.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Setup the settings menu
 *
 * @since       2.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings
 */
function simple_settings_demo_add_menu( $menu ) {
	$menu['type']        = 'menu';                                     // Can be set to 'submenu' or 'menu'. Defaults to 'menu'.
	$menu['parent']      = null;                                       // If 'type' is set to 'submenu', defines the parent menu to place our menu under. Defaults to 'options-general.php'.
	$menu['page_title']  = __( 'Simple Settings', 'simple-settings' ); // The page title. Defaults to 'Simple Settings'.
	$menu['show_title']  = true;                                       // Whether or not to display the title at the top of the page.
	$menu['menu_title']  = __( 'Simple Settings', 'simple-settings' ); // The menu title. Defaults to 'Simple Settings'.
	$menu['capability']  = 'manage_options';                           // The minimum capability required to access the settings panel. Defaults to 'manage_options'.
	$menu['icon']        = 'none';                                     // An (optional) icon for your menu item. Follows the same standards as the add_menu_page() function in WordPress.
	$menu['position']    = null;                                       // Where in the menu to display our new menu. Defaults to 'null' (bottom of the menu).
	$menu['allow_reset'] = true;                                       // Whether or not to allow users to reset the settings per-section.

	return $menu;
}
add_filter( 'simple_settings_demo_menu', 'simple_settings_demo_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       2.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs
 */
function simple_settings_demo_settings_tabs( $tabs ) {
	$tabs['core']    = __( 'Core', 'simple-settings' );
	$tabs['support'] = __( 'Support', 'simple-settings' );

	return $tabs;
}
add_filter( 'simple_settings_demo_settings_tabs', 'simple_settings_demo_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       2.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections
 */
function simple_settings_demo_registered_settings_sections( $sections ) {
	$sections = array(
		'core'    => array(
			'main'            => __( 'Intro', 'simple-settings' ),
			'checkbox_fields' => __( 'Checkbox/Radio Fields', 'simple-settings' ),
			'color_fields'    => __( 'Color Fields', 'simple-settings' ),
			'editor_fields'   => __( 'Editor Fields', 'simple-settings' ),
			'html_fields'     => __( 'HTML Fields', 'simple-settings' ),
			'select_fields'   => __( 'Select Fields', 'simple-settings' ),
			'text_fields'     => __( 'Text Fields', 'simple-settings' ),
			'upload_fields'   => __( 'Upload Fields', 'simple-settings' ),
		),
		'support' => array(),
	);

	return $sections;
}
add_filter( 'simple_settings_demo_registered_settings_sections', 'simple_settings_demo_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       2.0.0
 * @return      array $tabs The updated tabs
 */
function simple_settings_demo_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'simple_settings_demo_unsavable_tabs', 'simple_settings_demo_define_unsavable_tabs' );


/**
 * Disable save button on unsavable sections
 *
 * @since       2.0.0
 * @return      array $tabs The updated tabs
 */
function simple_settings_demo_define_unsavable_sections() {
	$sections = array( 'core/main' );

	return $sections;
}
add_filter( 'simple_settings_demo_unsavable_sections', 'simple_settings_demo_define_unsavable_sections' );


/**
 * Define our settings
 *
 * @since       2.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function simple_settings_demo_registered_settings( $settings ) {
	$new_settings = array(
		'core'    => array(
			'main'            => array(
				array(
					'id'            => 'field_header',
					'name'          => '<h2>' . __( 'Welcome!', 'simple-settings' ) . '</h2>',
					'desc'          => '',
					'type'          => 'header',
					'tooltip_title' => __( 'Tip', 'simple-settings' ),
					'tooltip_desc'  => __( 'This is a header field, and can be used with or without a tooltip.', 'simple-settings' ),
				),
				array(
					'id'   => 'core_intro',
					'name' => __( 'What is Simple Settings?', 'simple-settings' ),
					'desc' => '',
					'type' => 'hook',
				),
				array(
					'id'            => 'field_descriptive_text',
					'name'          => __( 'Descriptive Text Field', 'simple-settings' ),
					'desc'          => __( 'This is a descriptive text field.', 'simple-settings' ),
					'type'          => 'descriptive_text',
					'tooltip_title' => __( 'Tip', 'simple-settings' ),
					'tooltip_desc'  => __( 'This is a descriptive_text field, and can be used with or without a tooltip.', 'simple-settings' ),
				),
			),
			'checkbox_fields' => array(
				array(
					'id'            => 'field_checkbox',
					'name'          => __( 'Checkbox Field', 'simple-settings' ),
					'desc'          => __( 'This is a checkbox.', 'simple-settings' ),
					'type'          => 'checkbox',
					'class'         => '',
					'std'           => 'checked',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_multicheck',
					'name'          => __( 'Multicheck Field', 'simple-settings' ),
					'desc'          => __( 'This is a multicheck field.', 'simple-settings' ),
					'type'          => 'multicheck',
					'class'         => '',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
					'options'       => array(
						'option1' => __( 'Option 1', 'simple-settings' ),
						'option2' => __( 'Option 2', 'simple-settings' ),
						'option3' => __( 'Option 3', 'simple-settings' ),
					),
					'std'           => array(
						'option2' => 'checked',
					),
				),
				array(
					'id'            => 'field_radio',
					'name'          => __( 'Radio Field', 'simple-settings' ),
					'desc'          => __( 'This is a radio field.', 'simple-settings' ),
					'type'          => 'radio',
					'class'         => '',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
					'options'       => array(
						'option1' => __( 'Option 1', 'simple-settings' ),
						'option2' => __( 'Option 2', 'simple-settings' ),
						'option3' => __( 'Option 3', 'simple-settings' ),
					),
					'std'           => 'option2',
				),
			),
			'color_fields'    => array(
				array(
					'id'            => 'field_color',
					'name'          => __( 'Color Field', 'simple-settings' ),
					'desc'          => __( 'This is a color field.', 'simple-settings' ),
					'type'          => 'color',
					'class'         => '',
					'std'           => '#1793d1',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
			),
			'editor_fields'   => array(
				array(
					'id'             => 'field_editor',
					'name'           => __( 'Editor Field', 'simple-settings' ),
					'desc'           => __( 'This is an editor field.', 'simple-settings' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is a "standard" editor field.', 'simple-settings' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => true,
					'teeny'          => false,
					'editor_tinymce' => true,
					'editor_html'    => true,
					'default_editor' => '',
					'tooltip_title'  => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'             => 'field_editor_no_buttons',
					'name'           => __( 'Editor Field (No Buttons)', 'simple-settings' ),
					'desc'           => __( 'This is an editor field.', 'simple-settings' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is an editor field without buttons.', 'simple-settings' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => false,
					'teeny'          => false,
					'editor_tinymce' => true,
					'editor_html'    => true,
					'default_editor' => '',
					'tooltip_title'  => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'             => 'field_editor_tinymce_only',
					'name'           => __( 'Editor Field (TinyMCE Only)', 'simple-settings' ),
					'desc'           => __( 'This is an editor field.', 'simple-settings' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is an editor field with html disabled.', 'simple-settings' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => true,
					'teeny'          => true,
					'editor_tinymce' => true,
					'editor_html'    => false,
					'default_editor' => '',
					'tooltip_title'  => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'             => 'field_editor_html_only',
					'name'           => __( 'Editor Field (HTML Only)', 'simple-settings' ),
					'desc'           => __( 'This is an editor field.', 'simple-settings' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is an editor field with TinyMCE disabled.', 'simple-settings' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => true,
					'teeny'          => true,
					'editor_tinymce' => false,
					'editor_html'    => true,
					'default_editor' => '',
					'tooltip_title'  => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'             => 'field_editor_html_default',
					'name'           => __( 'Editor Field (HTML Default)', 'simple-settings' ),
					'desc'           => __( 'This is an editor field.', 'simple-settings' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is an editor field with quicktags selected by default.', 'simple-settings' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => true,
					'teeny'          => true,
					'editor_tinymce' => true,
					'editor_html'    => true,
					'default_editor' => 'html',
					'tooltip_title'  => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'simple-settings' ),
				),
			),
			'html_fields'     => array(
				array(
					'id'            => 'field_html',
					'name'          => __( 'HTML Field', 'simple-settings' ),
					'desc'          => __( 'This is an HTML (CodeMirror) field.', 'simple-settings' ),
					'type'          => 'html',
					'std'           => '',
					'mode'          => 'text/x-php',
					'line_numbers'  => true,
					'indent_size'   => 2,
					'indent_tabs'   => false,
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_html_no_line_numbers',
					'name'          => __( 'HTML Field (No Line Numbers)', 'simple-settings' ),
					'desc'          => __( 'This is an HTML field with line numbers disabled.', 'simple-settings' ),
					'type'          => 'html',
					'std'           => '',
					'mode'          => 'text/x-php',
					'line_numbers'  => false,
					'indent_size'   => 2,
					'indent_tabs'   => false,
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_html_custom_indent',
					'name'          => __( 'HTML Field (Custom Indentation)', 'simple-settings' ),
					'desc'          => __( 'This is an HTML field with indent set to 4 spaces.', 'simple-settings' ),
					'type'          => 'html',
					'std'           => '',
					'mode'          => 'text/x-php',
					'line_numbers'  => true,
					'indent_size'   => 4,
					'indent_tabs'   => false,
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
			),
			'select_fields'   => array(
				array(
					'id'            => 'field_select',
					'name'          => __( 'Select Field', 'simple-settings' ),
					'desc'          => __( 'This is a select field.', 'simple-settings' ),
					'type'          => 'select',
					'std'           => '',
					'placeholder'   => __( 'Pick something!', 'simple-settings' ),
					'select2'       => false,
					'multiple'      => false,
					'options'       => array(
						'option1' => __( 'Option 1', 'simple-settings' ),
						'option2' => __( 'Option 2', 'simple-settings' ),
						'option3' => __( 'Option 3', 'simple-settings' ),
					),
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_select_multi',
					'name'          => __( 'Select Field (Multiselect)', 'simple-settings' ),
					'desc'          => __( 'This is a select field with multi-selection enabled.', 'simple-settings' ),
					'type'          => 'select',
					'std'           => '',
					'placeholder'   => __( 'Pick something!', 'simple-settings' ),
					'select2'       => false,
					'multiple'      => true,
					'options'       => array(
						'option1' => __( 'Option 1', 'simple-settings' ),
						'option2' => __( 'Option 2', 'simple-settings' ),
						'option3' => __( 'Option 3', 'simple-settings' ),
					),
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_select_select2',
					'name'          => __( 'Select Field (Select2)', 'simple-settings' ),
					'desc'          => __( 'This is a select field with Select2 enabled.', 'simple-settings' ),
					'type'          => 'select',
					'std'           => '',
					'placeholder'   => __( 'Pick something!', 'simple-settings' ),
					'select2'       => true,
					'multiple'      => false,
					'options'       => array(
						'option1' => __( 'Option 1', 'simple-settings' ),
						'option2' => __( 'Option 2', 'simple-settings' ),
						'option3' => __( 'Option 3', 'simple-settings' ),
					),
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_select_select2_multi',
					'name'          => __( 'Select Field (Select2 Multiselect)', 'simple-settings' ),
					'desc'          => __( 'This is a select field with Select2 and multi-selection enabled.', 'simple-settings' ),
					'type'          => 'select',
					'std'           => '',
					'placeholder'   => __( 'Pick something!', 'simple-settings' ),
					'select2'       => true,
					'multiple'      => true,
					'options'       => array(
						'option1' => __( 'Option 1', 'simple-settings' ),
						'option2' => __( 'Option 2', 'simple-settings' ),
						'option3' => __( 'Option 3', 'simple-settings' ),
					),
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
			),
			'text_fields'     => array(
				array(
					'id'            => 'field_text',
					'name'          => __( 'Text Field', 'simple-settings' ),
					'desc'          => __( 'This is a text field.', 'simple-settings' ),
					'type'          => 'text',
					'std'           => '',
					'readonly'      => false,
					'disabled'      => false,
					'size'          => 'regular',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_text_read_only',
					'name'          => __( 'Text Field (Read Only)', 'simple-settings' ),
					'desc'          => __( 'This is a read only text field.', 'simple-settings' ),
					'type'          => 'text',
					'std'           => __( 'Don\'t edit me!', 'simple-settings' ),
					'readonly'      => true,
					'disabled'      => false,
					'size'          => 'regular',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_text_disabled',
					'name'          => __( 'Text Field (Disabled)', 'simple-settings' ),
					'desc'          => __( 'This is a large disabled text field.', 'simple-settings' ),
					'type'          => 'text',
					'std'           => __( 'Don\'t edit me!', 'simple-settings' ),
					'readonly'      => false,
					'disabled'      => true,
					'size'          => 'large',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_number',
					'name'          => __( 'Number Field', 'simple-settings' ),
					'desc'          => __( 'This is a small number field.', 'simple-settings' ),
					'type'          => 'number',
					'std'           => '0',
					'min'           => '0',
					'max'           => '999999',
					'step'          => '1',
					'size'          => 'small',
					'readonly'      => false,
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_number_step',
					'name'          => __( 'Number Field (Custom Step)', 'simple-settings' ),
					'desc'          => __( 'This is a number field with step set to ".1" and custom min/max values.', 'simple-settings' ),
					'type'          => 'number',
					'std'           => '0',
					'min'           => '-100',
					'max'           => '100',
					'step'          => '.1',
					'size'          => 'regular',
					'readonly'      => false,
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_number_read_only',
					'name'          => __( 'Number Field (Read Only)', 'simple-settings' ),
					'desc'          => __( 'This is a read only number field.', 'simple-settings' ),
					'type'          => 'number',
					'std'           => '0',
					'min'           => '0',
					'max'           => '999999',
					'step'          => '1',
					'size'          => 'small',
					'readonly'      => true,
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_textarea',
					'name'          => __( 'Textarea Field', 'simple-settings' ),
					'desc'          => __( 'This is a text area field.', 'simple-settings' ),
					'type'          => 'textarea',
					'std'           => '',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
				array(
					'id'            => 'field_password',
					'name'          => __( 'Password Field', 'simple-settings' ),
					'desc'          => __( 'This is a password field.', 'simple-settings' ),
					'type'          => 'password',
					'std'           => '',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
			),
			'upload_fields'   => array(
				array(
					'id'            => 'field_upload',
					'name'          => __( 'Upload Field', 'simple-settings' ),
					'desc'          => __( 'This is an upload field.', 'simple-settings' ),
					'type'          => 'upload',
					'std'           => '',
					'size'          => 'regular',
					'tooltip_title' => __( 'Tooltip Title', 'simple-settings' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'simple-settings' ),
				),
			),
		),
		'support' => array(
			array(
				'id'   => 'support_header',
				'name' => __( 'Simple Settings Support', 'simple-settings' ),
				'desc' => '',
				'type' => 'header',
			),
			array(
				'id'   => 'system_info',
				'name' => __( 'System Info', 'simple-settings' ),
				'desc' => '',
				'type' => 'sysinfo',
			),
		),
	);

	return array_merge( $settings, $new_settings );
}
add_filter( 'simple_settings_demo_registered_settings', 'simple_settings_demo_registered_settings' );


/**
 * Display our intro text (this uses the "hook" field!)
 *
 * @since       2.0.0
 * @return      void
 */
function simple_settings_display_intro() {
	echo '<div style="max-width: 750px;">';
	echo wp_kses_post( __( 'Depending on the size of a project, you may be able to get away with adding settings to an existing WordPress page (or the customizer). On the other end of the spectrum, you may need (or want) to implement a full-scale control panel like <a href="https://reduxframework.com" target="_blank">Redux</a>. However, what if your project is somewhere in the middle? Alternatively, what if you need a control panel but don\'t want the bulk that goes along with most? This need is something that I\'ve struggled with for some time. Simple Settings is a simple, standards-compliant library for creating theme and plugin settings panels with a difference. Built on the core of the excellent system used by <a href="https://easydigitaldownloads.com" target="_blank">Easy Digital Downloads</a>, Simple Settings strives to be simple, extensible, and maintain a native look and feel.', 'simple-settings' ) );
	echo '<br /><br />';
	echo wp_kses_post( __( 'Throughout the demo, we will be using tooltips to identify the individual fields shown. The above field is a <code>header</code> and this field is a <code>hook</code>. We will also be using the <code>descriptive_text</code> field type to show you how things are built.', 'simple-settings' ) );
	echo '</div>';
}
add_action( 'simple_settings_demo_core_intro', 'simple_settings_display_intro' );
